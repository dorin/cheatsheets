# Printing with CUPS

## `lp` options

### Two sided

    -o sides=two-sided-long-edge

### Two pages per page

    -o number-up=2

### Page list

    -P<pages>

## Resetting if `<printer> is not ready`

    cupsenable <printer>
