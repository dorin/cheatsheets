# SSH

## Key generation

    ssh-keygen -t rsa -C "email@addres.com" -b 4096

Change password for key:

    ssh-keygen -p <keyname>
