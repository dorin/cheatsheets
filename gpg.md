# GPG Crypto

## Encrypt file with passphrase

    gpg --symmetric <file>

### Decrypt

    gpg --output <out-file> --decrypt <in-file>
