# Manipulating tarballs

## Create (-c)

    tar -cf <new archive name> <files>
    # usually tar -cf directory.tar directory/

    # gzipped:
    tar -czf archive.tar.gz files...

## List contents (-t)

    tar -tf <archive>

## Extract (-x)

## Append (-r)
