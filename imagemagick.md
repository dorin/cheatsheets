# Manipulating images!

## Simple format conversion

    convert in.jpg out.png

## Resize

Fits image within given geometry, keeping the aspect ratio!

    convert in.jpg -resize 720x720 out.jpg

Force resize to be exactly the given geometry with `!`

    convert in.jpg --resize 720x720\! out.jpg

## Options with enum arguments

List possible option values

    convert -list <option>
    # e.g.
    convert -list dither
