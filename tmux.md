# Tmux

## Sessions

    (/) switch between sessions

## Window management

    .N  move window to index N
    w   choose window interactively

## Pane management

    q   display pane indices
