# Downloading with wget

## --page-requisites

Gets all files necessary to display given page (images, sounds, CSS, ...)

    wget -p <url>

## Recursive

Defaults to 5 levels deep.

    wget -r <url>
    wget -r -l <depth> <url>

Accept or reject links with patterns (useful to download files from a file
listing, where the files follow a naming pattern).

    wget -r -A <file1,file2> # can use *, ?, []
    wget -r -R <file1,file2> # can use *, ?, []
    wget -r --accept-regex <regex>
    wget -r --reject-regex <regex>
