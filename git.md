# Git tips and tricks

## Setup name and email

    git config --global user.name "Name"
    git config --global user.email "name@place.com"

## `git bisect`

    git bisect start
    git bisect new
    git checkout <some commit a while ago>
    git bisect old
    git bisect run <command that returns 1 on old, 0 on new>
