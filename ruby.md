# Ruby stuff

## Bundler

### Install packages locally

    bundle install --path vendor/bundle

## Gemfile

### Basic

    source 'https://rubygems.org'

    gem 'gemname'
