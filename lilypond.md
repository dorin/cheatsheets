# Lilypond stuff

Docs: `info LilyPond`.

## Fine-grained page size

    #(set! paper-alist (cons '("custom size" . (cons (* 3.8 cm) (* 1.6 cm)))
                             paper-alist))
    \paper {
      #(set-paper-size "custom size")
    }

## Remove footer

    \header {
        tagline = ##f
    }

## Rhythm stuff

### Setting beat divisions

    \overrideTimeSignatureSettings
          4/4        % timeSignatureFraction
          1/4        % baseMomentFraction
          #'(3 1)    % beatStructure
          #'()       % beamExceptions
    \time 4/4
    \repeat unfold 8 { c8 } |
    \revertTimeSignatureSettings 4/4 % reverts to defaults
    \time 4/4
    \repeat unfold 8 { c8 } |

### Pickup measure

    \time 3/4
    \partial 8
    e8 | a4 c8 b c4 |
