# ffmpeg tricks

## Cut

    ffmpeg -i in.mp4 -codec:v copy -codec:a copy -ss START_TIME -t DURATION out.mp4
    ffmpeg -ss START_TIME -t DURATION -i in.mp4 -codec:v copy -codec:a copy out.mp4

## Resize

    ffmpeg -i in.mp4 -s 720x480 out.mp4
    # -1 means to automatically preserve aspect ratio
    ffmpeg -i in.mp4 -filter:v scale=720:-1 out.mp4

## Rotate

    # 90 degrees clockwise
    ffmpeg -i in.mov -vf "transpose=1" out.mov
    # 180 degrees
    ffmpeg -vf "transpose=2,transpose=2"

For the transpose parameter you can pass:

    0 = 90CounterCLockwise and Vertical Flip (default)
    1 = 90Clockwise
    2 = 90CounterClockwise
    3 = 90Clockwise and Vertical Flip

## Record screen

    ffmpeg -f x11grab -s 1920x1080 -i :0.0 out.mkv

## Record from webcam

    ffmpeg -i /dev/video0 out.mkv

## Record sound

    ffmpeg -f alsa -i default out.flac
