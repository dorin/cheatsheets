# Bash scripting

## String manipulation

Remove shortest from end

    ${VAR%substr}
    # e.g.
    ${FILEPATH%.*}.out   # change extension from whatever to .out

Remove longest from start

    ${VAR##substr}
    # e.g.
    ${FILEPATH##*/}      # get only file name portion

%% - longst from end, # - shortest from start

## Control flow

Loop over files

    for file in glob/*; do something $file; done
